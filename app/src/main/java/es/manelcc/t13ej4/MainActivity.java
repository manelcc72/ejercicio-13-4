package es.manelcc.t13ej4;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.RadioGroup;

public class MainActivity extends AppCompatActivity implements RadioGroup.OnCheckedChangeListener {
    private RadioGroup mRGSelector;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mRGSelector = (RadioGroup) findViewById(R.id.rbGroup);
        mRGSelector.setOnCheckedChangeListener(this);
    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {

        int flags = 0;
        switch (mRGSelector.getCheckedRadioButtonId()) {
            case R.id.pantallaNormal:
                flags = View.SYSTEM_UI_FLAG_VISIBLE;
                break;
            case R.id.perfilBajo:
                flags = View.SYSTEM_UI_FLAG_LOW_PROFILE;
                break;
            case R.id.escondeBarraNavegacion:
                flags = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION;
                break;
            case R.id.escondeBarraEstado:
                flags = View.SYSTEM_UI_FLAG_FULLSCREEN;
                break;
            case R.id.pantallaCompleta:
                flags = View.SYSTEM_UI_FLAG_FULLSCREEN | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION;
                break;
        }
        group.setSystemUiVisibility(flags);


    }
}
